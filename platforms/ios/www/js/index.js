/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');  

        
            $.ajax({
        url: serverURL + "/gets3FilesMobile",
        type: "GET",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(res) {

                   $("#clientToken").val(res.braintreeaccesstoken);
        },
        error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();},
        complete: function() {


        }
    });
 
	// window.location.href = 'dashboard.html';
   /*   var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
      myDB.transaction(function(transaction) {
          transaction.executeSql('CREATE TABLE IF NOT EXISTS aeomSettings (id integer primary key, key1 text, key2 text, key3 text,key4 text,key5 text)', [],
              function(tx, result) {
               //console.log("Table created successfully:" + JSON.stringify(result));
              },
              function(error) {
                   alert(error);
              });
      }); */
        /* $.ajax({
                                           url: serverURL+"/getActivationCode",
                                          type: "POST",
                                          data: JSON.stringify({
                                              "getActivationCode": {
                                                  "model": device.model,
                                                  "manufacturer": device.manufacturer,
                                                  "device_name": device.platform,
                                                  "device_id": device.uuid,
                                                  "device_mac_address": '',
                                                  "brand_name": device.platform,
                                                  "host_name": '',
                                                  "display_name": device.platform,
                                                  "serial_number": device.serial 
                                               }
                                          }),
                                          dataType: "json",
                                          contentType: "application/json; charset=utf-8",
                                          traditional: true,
                                          success: function(data) {
                                                var Pairingcode=data.code;
                                                $("#pairCode").html("");
                                               if(data.pair_devce=='pending'){
                                                    showPage('PairingScreen');
                                                    console.log(Pairingcode);
                                                    $("#user_Id").val('');   
                                                      for(i = 0; i < Pairingcode.length; i++ )
                                                    {
                                                       $("#pairCode").append('<li>'+Pairingcode.charAt(i)+'</li>'); 
                                                    }
                                                      checkPairingStatus = setInterval(function(){ getParingStatus() }, 5000);

                                                }
                                               else
                                               {
                                                    $("#user_Id").val(data.user_id); 
                                                   showPage('HomePage');
                                                        $('.navbar').show();
                                               }
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                             // alert(errorThrown);
                                          }

                                      });*/


      // Temporally commented  due to disabled user login method
    /* myDB.transaction(function(transaction) {
          transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
              var len = results.rows.length,
                  i;
              if (len > 0) {
                  for (i = 0; i < len; i++) {
                       $('.joinbtn').hide();

                      if(results.rows.item(i).key4)
                        {
                          $('#profile_pic').attr('src', results.rows.item(i).key4);

                        }

                     $('.uAvatar').show();
                     $('.preloader').hide();
                     showPage('HomePage');
                     

                  }
              }
          }, null);
      });  */

      // This need to be remove when user login methos is enabled 
         
      /*  var lock = new Auth0Lock(
            // All these properties are set in auth0-variables.js
            AUTH0_CLIENT_ID,
            AUTH0_DOMAIN 
         );
        var userProfile; 
        $('.joinbtn').click(function(e) {
          showPage('HomePage');
            e.preventDefault();
            lock.show({ socialBigButtons: true},function(err, profile, token) {
                if (err) {
                    // Error callback
                    alert("There was an error logging in");
                    showPage('HomePage');
                } else {
					   

                  var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
                  myDB.transaction(function(tx) {
                          var qry = "DELETE FROM aeomSettings";
                          tx.executeSql(qry, function() {
                              // console.log("Delete success");
                          });
                      }, function errorCB(err) {
                           showAlert("Error processing SQL: " + err.code);
                          lock.hide();
                          showPage('HomePage');
                      },
                      function() {
                          var deviceID = device.uuid;

                          myDB.transaction(function(transaction) {
                              var executeQuery = "INSERT INTO aeomSettings (key1, key2,key4) VALUES (?,?,?)";
                              var ProfilePIC=profile.picture;
                              transaction.executeSql(executeQuery, [token, profile.name, profile.picture], function(tx, result) {
                                       $.ajax({
                                           url: serverURL+"/mobileSignIn",
                                          type: "POST",
                                          data: JSON.stringify({
                                              "mobileSignIn": {
                                                  "token": token,
                                                  "id": deviceID
                                              }
                                          }),
                                          dataType: "json",
                                          contentType: "application/json; charset=utf-8",
                                          traditional: true,
                                          success: function(data) {
                                               UpdateID(data.accountResult._id,ProfilePIC);
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                              lock.hide();
                                          },
                                          complete: function() {

                                          }
                                      });
                                  },
                                  function(error) {
                                       lock.hide();
                                   });
                          });

                      });




                    
                }
            });
            //    lock.show({socialBigButtons: true}); 
        }); */


  

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

    }
};



app.initialize();

  function UpdateID(id,PP) {
           var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
      myDB.transaction(function(tx) {
          var qry = "UPDATE aeomSettings SET key3='" + id + "'";
          tx.executeSql(qry, function() {

          });
      }, function errorCB(err) {
           // showAlert("Error processing SQL: " + err.code);
          //$("#loading").hide();
      }, function() {
          //window.location.href = 'dashboard.html';
                      $('.joinbtn').hide();
                      if(PP) 
                        {
                         $('#profile_pic').attr('src', PP);
                       }
                      $('.uAvatar').show();
                      $('.preloader').hide();
                      showPage('HomePage');



      });

  }

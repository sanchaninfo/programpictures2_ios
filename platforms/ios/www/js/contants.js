  /*
    File Name   :      auth0-variables.js
    Project     :      AEOM Android App
    Copyright (c)      www.alleyezonmethemovie.com
    author      :      Prasanna 
    license     :   
    version     :      0.0.1 
    Created on  :      March 23, ‎2017
    Description :      This file contains Auth0 App ID details                         . 
    Organisation:      Peafowl inc.  
    */
var AUTH0_CLIENT_ID='LrYiwrEdTf87w44KXrOslRY804tFvQX8'; 
var AUTH0_DOMAIN='damedashstudios.auth0.com'; 
var AUTH0_CALLBACK_URL=location.href;
var serverURL = "http://www.theprogrampictures.com";
var s3URL="https://s3.amazonaws.com/peafowlaeom/programpictures";
 var cdnPath="http://aeom-static-assets.cdn.alleyezonme.movie/aeommovie";
var token = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI2Yjg1NTdiOTMxMzk4ZTdjZWNlMjEzNTYwZmM0OTU1OTk2NTZhNTRjZmYxZDM4N2Q3NmIwMDM0NTIwMTgyNTNhfGNyZWF0ZWRfYXQ9MjAxNi0xMS0xMFQwMzo1NTo1OC4wMzg1NTUxNTUrMDAwMFx1MDAyNm1lcmNoYW50X2lkPXkydjZqem53cXNtZHd6enpcdTAwMjZwdWJsaWNfa2V5PTVuN3dmeHRubXFwOGZ3c3giLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMveTJ2Nmp6bndxc21kd3p6ei9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL3kydjZqem53cXNtZHd6enovY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tL3kydjZqem53cXNtZHd6enoifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoic2FuY2hhbiIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6InNhbmNoYW4iLCJjdXJyZW5jeUlzb0NvZGUiOiJVU0QifSwiY29pbmJhc2VFbmFibGVkIjpmYWxzZSwibWVyY2hhbnRJZCI6InkydjZqem53cXNtZHd6enoiLCJ2ZW5tbyI6Im9mZiJ9";

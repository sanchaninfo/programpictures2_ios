var largImg,ImgTtile,imgIDs;
var slCnt=0;
var chngCnt=0;
var castCnt=0;
var aftGal=0;
var galCurrentImage;
var SwCnt=0;
var eventCnt=0;
var products = [];
var events = [];
var selectedClr ='';
var selectedProductId='';
var selectedUnitPrice='';
var productImage='';
var ci="";
 function loadPage(id) {
    $("#loader-div").show();
    window.location.href = id;

}

$("document").ready(function() {

                $( "#qty2" ).click(function() {
                  $("#qty1").html(parseInt($("#qty1").html())+1);
                         $("#prodPrice").html((parseFloat($("#productPrice").val())*parseFloat($("#qty1").html())).toFixed(2));
                });
                $( "#qty0" ).click(function() {
                  if(parseInt($("#qty1").html())>1){
                     $("#qty1").html(parseInt($("#qty1").html())-1);
                      $("#prodPrice").html(  (parseFloat($("#productPrice").val())*parseFloat($("#qty1").html())).toFixed(2));
                  }
                 

                });


            })

function showPopup(id,sk)
{

 
     if(id=='1')
    {
            $("#gallery").html('');
           $.each(largImg, function(key, value) {
            if(key==0) { galCurrentImage=value;}
          $("#gallery").append('<div><img src="img/preloader2.svg" class="preloader1"/><img class="img-responsive" data-lazy="' + value + '"> <h4 class="imgTitle abspos btmpos fullwidth">'+ImgTtile[key]+'</h4></div>');
                   if(slCnt==1) {
                     $('.regular').slick('unslick');

                      
                     slCnt=0;}

                 if ((largImg.length == key + 1)) { 

                      if( slCnt==0) {
                        $(".regular").slick({
                        dots: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        prevArrow: false,
                        nextArrow: false
                      });
                    slCnt++;
                     $('.regular').slick('slickGoTo', sk+aftGal,false); 

                        if(chngCnt==0) 
                        {
 
                            $('.regular').on('afterChange', function(event, slick, currentSlide, nextSlide){

                         if($('.slick-active img:eq(1)').attr('data-lazy'))
                         {
                              ci=largImg.indexOf( $('.slick-active img:eq(1)').attr('data-lazy') ) ;
                         }
                         else
                         {
                            ci=largImg.indexOf( $('.slick-active img:eq(1)').attr('src') ) ;
                         }
 

                             $.ajax({
                            url: serverURL + "/Views",
                            type: "POST",
                            data: JSON.stringify({
                                  "Views": {
                                      "id": imgIDs[ci],
                                      "type": "photo",
                                      "count": "1"
                                  }
                              }),
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            success: function(data) {
                                       // $("#v_"+data.views.sourceId).html(parseInt(data.views.count+1));
                                        $("#v_"+data.views.sourceId).html('<img src="img/stats/view.png" class="statsIcon"/><span class="statsTxt">'+parseInt(data.views.count+1)+'</span>');
                                        
                                
                            },
                            error: function(jqXHR, textStatus, errorThrown) {  },
                            
                        });  



                       });
                          chngCnt++;
                        }
                       


                       }  


             

                  };      


             
              $('.Videoprev').click(function(){
 				     $(".regular").slick('slickPrev');
				});
              $('.Videonext').click(function(){
 				     $(".regular").slick('slickNext');
				});

             });
              aftGal++;
                 $('#photoPopUpPage').show(); 
                 
    }
    else
    {
       
       $('#photoPopUpPage').fadeOut(500);   

    }


}
function showe1()
{  
          $("#e1").show(); 
          $("#e2").hide();    
          $("#e3").hide();  
          $("#e4").hide();  
          $("#e5").hide();  
}
function back2Events()
{
  showe2($("#selectedEvent").val()); 
}
function showe2(id)
{   
          $("#selectedEvent").val(id);
          $("#e1").hide();    
          $("#e2").show();    
          $("#e3").hide();  
          $("#e4").hide(); 
          $("#e5").hide();   
          $(".evPrice").html(events[0][id].price);
          $(".evt_img").html('<img src="'+s3URL+events[0][id].images[0]+'">'); 
          $(".prt_lft").html('<img src="'+s3URL+events[0][id].images[0]+'">'); 
          $("#evTitle").html(events[0][id].title);
          $("#evenue").html(events[0][id].venue);


}
function showe3()
{     
          $("#e1").hide();    
          $("#e2").hide();    
          $("#e3").show();  
          $("#e4").hide();  
          var ep= parseFloat($("#eventPrice").html());
          var nog=  parseInt($('#prdSize option:selected').val());
          $("#nog").html(nog);
          $("#top").html(nog*ep);
          $("#e5").hide();  
 }
function showe4()
{     
          $("#e1").hide();    
          $("#e2").hide();    
          $("#e3").hide();  
          $("#e4").show(); 
          getAddress();
          $("#e5").hide(); 
          $("#continue_payment").html('<input class="smt confirmred botmarg10"  onClick="javascript:makePayment()" id="ContinuePayment" type="button" value="Continue to Payment">'); 
}
function showe5()
{  
          $("#e1").hide();    
          $("#e2").hide();    
          $("#e3").hide();  
          $("#e4").hide();  
          $("#e5").show();  
}


function getAddress(){
    getuserdetails();
  /*var myDB = window.sqlitePlugin.openDatabase({ name: 'AeomDB.db', location: 'default' }, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
           // var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
               // var UserID = results.rows.item(i).key3;

                
*/
          var UserID = $("#user_Id").val();
                if(UserID) {
                 $.ajax({


                    type: "POST",
                    url: serverURL + "/fetchAddressStore",
                    data: JSON.stringify({
                        "fetchAddressStore": {
                            "user_id": UserID,
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {

                        $("#loading").hide();
                      
                        if(data[0]) {
                          var returnedData = $.grep(data[0].address, function (element, index) {
                        return element.defaultAddress == true;
                    });
                       //$("#email").val(returnedData[0].billingAddress.firstName);
                       $("#name").val(returnedData[0].billingAddress.firstName);
                       $("#billingAddress").val(returnedData[0].billingAddress.streetAddress);
                       //$("#bextendedAddress").val(returnedData[0].billingAddress.locality);
                       $("#blocality").val(returnedData[0].billingAddress.locality);
                       $("#bregion").val(returnedData[0].billingAddress.region);
                       $("#bpostalCode").val(returnedData[0].billingAddress.postalCode);

                       $("#name_shipping").val(returnedData[0].billingAddress.firstName);
                       $("#billingAddress_shipping").val(returnedData[0].billingAddress.streetAddress);
                       //$("#bextendedAddress").val(returnedData[0].billingAddress.locality);
                       $("#blocality_shipping").val(returnedData[0].billingAddress.locality);
                       $("#bregion_shipping").val(returnedData[0].billingAddress.region);
                       $("#bpostalCode_shipping").val(returnedData[0].billingAddress.postalCode);

                       $("#name_shipping1").val(returnedData[0].billingAddress.firstName);
                       $("#billingAddress_shipping1").val(returnedData[0].billingAddress.streetAddress);
                       //$("#bextendedAddress").val(returnedData[0].billingAddress.locality);
                       $("#blocality_shipping1").val(returnedData[0].billingAddress.locality);
                       $("#bregion_shipping1").val(returnedData[0].billingAddress.region);
                       $("#bpostalCode_shipping1").val(returnedData[0].billingAddress.postalCode);
                        }
                      
                                            
                       getuserdetails();
 
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
 
                    },
                    complete: function() {}
                
                });
                  } else{ 
                   showParingScreen();
                  }


/*

                   } else {
                console.log('No user found.');
                $('.preloader').hide();
                $( ".joinbtn" ).click(); 

            }
        }, null);
    }); */
}
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function proceedPayment()
{
    var email_shipping=$("#email_shipping").val();
    var name_shipping=$("#name_shipping").val();
    var billingAddress_shipping=$("#billingAddress_shipping").val();
    var blocality_shipping=$("#blocality_shipping").val();
    var bregion_shipping=$("#bregion_shipping").val();
    var bregion=$("#bregion").val();
    var bpostalCode_shipping=$("#bpostalCode_shipping").val();

    $("#email_shipping").css("background-color","#fff");
    $("#name_shipping").css("background-color","#fff");
    $("#blocality_shipping").css("background-color","#fff");
    $("#bregion_shipping").css("background-color","#fff");
    $("#bpostalCode_shipping").css("background-color","#fff");
    $("#billingAddress_shipping").css("background-color","#fff");

     var billingAddress_shipping1=$("#billingAddress_shipping1").val();
    var blocality_shipping1=$("#blocality_shipping1").val();
    var bregion_shipping1=$("#bregion_shipping1").val();
    var bpostalCode_shipping1=$("#bpostalCode_shipping1").val();

    $("#email_shipping1").css("background-color","#fff");
    $("#name_shipping1").css("background-color","#fff");
    $("#blocality_shipping1").css("background-color","#fff");
    $("#bregion_shipping1").css("background-color","#fff");
    $("#bpostalCode_shipping1").css("background-color","#fff");
    $("#billingAddress_shipping1").css("background-color","#fff");

    if(email_shipping==''){
      
      $("#email_shipping").css("background-color","rgb(241, 233, 174)");
      $("#email_shipping").focus();
      return false;
      
    }
    else if(isEmail(email_shipping)==false)
    {
       $("#email_shipping").css("background-color","rgb(241, 233, 174)");
       $("#email_shipping").focus();
      return false;

    }
        else if(name_shipping=='')
    {
       $("#name_shipping").css("background-color","rgb(241, 233, 174)");
       $("#name_shipping").focus();
      return false;

    }
      else if(billingAddress_shipping=='')
    {
       $("#billingAddress_shipping").css("background-color","rgb(241, 233, 174)");
       $("#billingAddress_shipping").focus();
      return false;

    }

        else if(blocality_shipping=='')
    {
       $("#blocality_shipping").css("background-color","rgb(241, 233, 174)");
       $("#blocality_shipping").focus();
      return false;

    }
        else if(bregion_shipping=='')
    {
       $("#bregion_shipping").css("background-color","rgb(241, 233, 174)");
       $("#bregion_shipping").focus();
      return false;

    }
          else if(bpostalCode_shipping=='')
    {
       $("#bpostalCode_shipping").css("background-color","rgb(241, 233, 174)");
       $("#bpostalCode_shipping").focus();
      return false;

    }

    
    if($("#sameaddress").is( ":checked" )==false)
      {       

         


         if(billingAddress_shipping1=='')
              {
                  $("#billingAddress_shipping1").css("background-color","rgb(241, 233, 174)");
                 $("#billingAddress_shipping1").focus();
                 return false;

              }

                  else if(blocality_shipping1=='')
              {
                  $("#blocality_shipping1").css("background-color","rgb(241, 233, 174)");
                 $("#blocality_shipping1").focus();
                return false;

              }
           else if(bregion_shipping1=='')
              {
                  $("#bregion_shipping1").css("background-color","rgb(241, 233, 174)");
                 $("#bregion_shipping1").focus();
                return false;

              }
                 else if(bpostalCode_shipping1=='')
              {
                  $("#bpostalCode_shipping1").css("background-color","rgb(241, 233, 174)");
                 $("#bpostalCode_shipping1").focus();
                return false;

              }

        
      }
       
      

        // Payment initiate


 //$("#loading").show();
  // token declared in constants
  $('.preloaderFixed').show();
    BraintreePlugin.initialize($("#clientToken").val(),
        function () {
       //  $("#loading").show();
       $('.preloaderFixed').hide();
         var options = {
        //amount: parseFloat($("#top").html()),
       // primaryDescription: "Your Item"
    };

  BraintreePlugin.presentDropInPaymentUI(options, function (result) {
 
      if (result.userCancelled) {
         // console.debug("User cancelled payment dialog.");
          $('.preloaderFixed').hide();
      }
      else {
             $('.preloaderFixed').show();
 
                         $.ajax({
          url: serverURL + "/payWithoutCVV",
          type: "POST",
          data: '{ "payWithoutCVV": { "email": "'+email_shipping+'", "billingAddress":{"firstName":"'+name_shipping+'","lastName":"","streetAddress":"'+billingAddress_shipping+'","extendedAddress":"","locality":"'+blocality_shipping+'","region":"'+bregion_shipping+'","postalCode":"'+bpostalCode_shipping+'","countryCodeAlpha2":"US"}, "shippingAddress": {"firstName":"'+name_shipping+'","lastName":"","streetAddress":"'+billingAddress_shipping1+'","extendedAddress":"","locality":"'+blocality_shipping1+'","region":"'+bregion_shipping1+'","postalCode":"'+bpostalCode_shipping+'","countryCodeAlpha2":"US"}, "userName": "'+$("#userName").val()+'", "productName": "'+$("#prodTitle2").html()+'", "productPrice": "'+$("#productPrice").val()+'", "productColor": "'+selectedClr+'", "productSize": "'+$( "#DprdSize option:selected" ).val()+'", "productQuantity": "'+$("#qty1").html()+'", "productDeliveryCharge": "Free", "userPhoneNumber": "", "productImage": "'+productImage+'", "productId": "'+selectedProductId+'", "payment_method_nonce":"'+result.nonce+'", "userId": "'+$("#user_Id").val()+'", "customerId": "'+$("#customer_id").val()+'" ,"clearancePrice": "'+selectedUnitPrice+'" } }',
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          traditional: true,
          success: function(res) {
                    // console.log('resposnse:'+JSON.stringify(res));
   if(res.success==true) { $("#orderStatus").html('Thank you for your order!'); 

       $("#prod_order").html(res.transaction['orderId']);
                   $("#prod_order_date").html(res.order['orderDate']);
                   $("#prod_img").html('<img src="'+res.order['productImage']+'" >');
                   $("#prod_order_price").html(res.transaction['amount']);
                   $("#prod_order_color").css("background-color",res.order['productColor']);
                   $("#prod_order_size").html(res.order['productSize']);
                   $("#prod_order_qty").html(res.order['productQuantity']);
                   $("#prod_order_billing_addr").html(res.transaction['billing'].streetAddress+','+res.transaction['billing'].locality+','+res.transaction['billing'].region+'  '+res.transaction['billing'].postalCode);
                  
                     if($("#sameaddress").is( ":checked" )==true)
					  {
					      
                  	$("#prod_order_shipping_addr").html(res.transaction['billing'].streetAddress+','+res.transaction['billing'].locality+','+res.transaction['billing'].region+'  '+res.transaction['billing'].postalCode);
					  }
					  else
					  {
					     
                  $("#prod_order_shipping_addr").html(res.transaction['shipping'].streetAddress+','+res.transaction['shipping'].locality+','+res.transaction['shipping'].region+'  '+res.transaction['shipping'].postalCode);

					  }

                    

 } else { $("#orderStatus").html('Order failed!'); 

} 
                   
               

                      showPage('orderConfirm')
                   $('.preloaderFixed').hide();
          },
          error: function(jqXHR, textStatus, errorThrown) { 
            $('.preloaderFixed').hide();
           // alert(errorThrown);
          },
          complete: function() {


          }
      });  



      }
  });
  
    },
    function (error) {
     //console.error(error);
      });  

 

    }
function makePayment()
{
     var email=$("#email").val();
    var name=$("#name").val();
    var billingAddress=$("#billingAddress").val();
    var blocality=$("#blocality").val();
    var bregion=$("#bregion").val();
    var bpostalCode=$("#bpostalCode").val();
    $("#email").css("background-color","#fff");
    $("#name").css("background-color","#fff");
    $("#blocality").css("background-color","#fff");
    $("#bregion").css("background-color","#fff");
    $("#bpostalCode").css("background-color","#fff");
    $("#billingAddress").css("background-color","#fff");
    if(email==''){
      
      $("#email").css("background-color","rgb(241, 233, 174)");
      $("#email").focus();
      return false;
      
    }
    else if(isEmail(email)==false)
    {
       $("#email").css("background-color","rgb(241, 233, 174)");
       $("#email").focus();
      return false;

    }
        else if(name=='')
    {
       $("#name").css("background-color","rgb(241, 233, 174)");
       $("#name").focus();
      return false;

    }
      else if(billingAddress=='')
    {
       $("#billingAddress").css("background-color","rgb(241, 233, 174)");
       $("#billingAddress").focus();
      return false;

    }

           else if(blocality=='')
    {
       $("#blocality").css("background-color","rgb(241, 233, 174)");
       $("#blocality").focus();
      return false;

    }

          else if(bregion=='')
    {
       $("#bregion").css("background-color","rgb(241, 233, 174)");
       $("#bregion").focus();
      return false;

    }

          else if(bpostalCode=='')
    {
       $("#bpostalCode").css("background-color","rgb(241, 233, 174)");
       $("#bpostalCode").focus();
      return false;

    }



  
 $("#loading").show();
 
    BraintreePlugin.initialize($("#clientToken").val(),
        function () {
               $("#loading").show();
        var options = {
        //amount: parseFloat($("#top").html()),
       // primaryDescription: "Your Item"
    };

  BraintreePlugin.presentDropInPaymentUI(options, function (result) {

      if (result.userCancelled) {
         // console.debug("User cancelled payment dialog.");
      }
      else {
          // console.debug("Payment Result.", result);
            $('.preloader').show();
            $("#e4").hide();
              $.ajax({
          url: serverURL + "/payWithParty",
          type: "POST",
          data: '{"payWithParty":{"email":"'+$("#email").val()+'","billingAddress":{"firstName":"'+$("#name").val()+'","lastName":"","streetAddress":"'+$("#billingAddress").val()+'","extendedAddress":"","locality":"'+$("#blocality").val()+'","region":"'+$("#bregion").val()+'","postalCode":"'+$("#bpostalCode").val()+'","countryCodeAlpha2":"US"},"shippingAddress":{"firstName":"'+$("#name").val()+'","lastName":"","streetAddress":"'+$("#billingAddress").val()+'","extendedAddress":"","locality":"'+$("#blocality").val()+'","region":"'+$("#bregion").val()+'","postalCode":"'+$("#bpostalCode").val()+'","countryCodeAlpha2":"US"},"userName":"'+$("#userName").val()+'","productName":"'+s3URL+events[0][$("#selectedEvent").val()].title+'","productPrice":'+parseFloat($("#top").html())+',"productColor":"","productSize":"","productQuantity":"'+parseFloat($("#nog").html())+'","productDeliveryCharge":"Free","userPhoneNumber":"","productImage":"'+s3URL+events[0][$("#selectedEvent").val()].images[0]+'","productId":"","clearancePrice":"'+parseFloat($("#top").html())+'","type":"party","payment_method_nonce":"'+result.nonce+'","userId":"'+$("#user_Id").val()+'","customerId":"'+$("#customer_id").val()+'"}}',
          dataType: "json",
          contentType: "application/json; charset=utf-8",
          traditional: true,
          success: function(res) {
                    showe5();
                   $('.preloader').hide();
                  $("#gues").html(parseFloat($("#nog").html()));
                  $("#guesPrice").html('$'+parseFloat($("#top").html()));
                  $("#orderId").html(res.order.orderId);
                  $("#orderDate").html(res.order.orderDate);
                  $("#add1").html(res.order.deliveryAddressLine1);
                  $("#add2").html(res.order.deliveryAddressLine2);
                  $("#add3").html(res.order.deliveryAddressLine3);
          },
          error: function(jqXHR, textStatus, errorThrown) { 
            $('.preloader').hide();
          },
          complete: function() {


          }
      });  



      }
  });
  
    },
    function (error) { 
     // console.error(error);
       });

}
 function showPage1(id)
 {
   $(".navbar").show();
  $(".commonClass").hide();
  $("#VideosSubPage").show();
  getAssetData(id);
 }

function showPage(id)
{   
   console.log(id);
  $(".navbar").show();

  $('.closebt_cont').removeClass("change"); 
  $("#head_navigation").hide(500);  
  $(".commonClass").hide();

     if(id=='Photos')
  {
      $("#GalleryDetailsPage").show();
      getPhotos();
  }

  else if(id=='AccountDetails')
  {
     $("#"+id).show();
     $("#devicePairBlock").html('<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 nopad" ><div class="col-lg-12 col-md-12 nopad formdevice toggleHide" ><div class="form-group" ><input type="text" class="pairinput inputstyle inputcode form-control" name="devicecode" id="devicecode" placeholder="Enter Code" ></div><input id="deviceAdd" onclick="addbuttonclick()" class="btn btn-info inputcodebtn" type="button" value="Add" ><br ></div><p class="nopad redtxt" ></p></div><div id="devicedetails"></div>');
      getuserdetails();
      getdevicepaired();
  }

else if(id=='StorePage')
  {
     $("#"+id).show();
        getProducts();
  }
else if(id=='EventPage')
  {
     $("#"+id).show();
     gets3EventsData();

  }
   else if(id=='StoryPage'  ||  id=='Films' || id=='SoundTrackPage' || id=='HomePage' || id=='PairingScreen' || id=='StoreDetails' || id=='shippingAddress' ||id=='orderConfirm' )
  {
      $("#"+id).show();
     if(id=='PairingScreen') {
        $(".navbar").hide();
     } 
  }
  else  
  {
      $("#VideosPage").show();
       getCarouselsList(id);
      // console.log("AAAA:"+id)
  }
 


 

  /*if(id=='GalleryPage')
  {
    gets3Files('GalleryPage');

  } */

if(id=='Films')
  {
    getCarousels();
    
  }

  else   if(id=='SoundTrackPage')
  {
     getCarouselsList("Music");

  }
 
 


}
 

function showTrackDetails()
{

  $("#trackList").hide();
  setTimeout(function(){  $("#trackDteails").show();}, 100)
  
}

function hideTrackDetails()
{

  $("#trackDteails").hide();
  setTimeout(function(){  $("#trackList").show();}, 100)
  
}
function getProducts() {
   $('.preloader').show();
   $('#storeList').html('');
   var prdimage;
    $.ajax({
        url: serverURL + "/gets3productData",
        type: "GET",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(data) {
               
           $('.preloader').hide();
          products=[];
          products.push(data['dataa'][0]['carousels'][0]['products']);
           $.each(data['dataa'][0]['carousels'][0]['products'], function(key, value) {
            var storeIMG=s3URL+value.images[0].thumb;
            $('#storeList').append('<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 lslidee "><a href=javascript:productDetails("'+key+'") class="shopImg"><img width="100%" class="liImage"   id="PrdBg'+key+'" src="img/white200.svg"><h5 class="ItemName">'+value.title+'</h5> </a></div>');

              prdimage = new Image();
        if (storeIMG != null)
        prdimage.src = storeIMG;       
        
        $(prdimage).load(function(){
 
             $("#PrdBg"+key).fadeOut(50);
             setTimeout(function(){$("#PrdBg"+key).attr("src",storeIMG).fadeIn(400);},50);
              
         }); 

        
   


          });
        },
        error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();},
        complete: function() {
           


        }
    });
}
function sameAddr()
{
  if($("#sameaddress").is( ":checked" )==true)
  {
      $("#saddr").hide();
  }
  else
  {
      $("#saddr").show();

  }
}
function productDetails(id)
{
 
     $("#StoreDetails").scrollTop();
    // $("#StoreDetails").animate({ scrollTop: 0 }, "fast");  
     // setTimeout(function(){showPage('StoreDetails');},500);
    //console.log("products:"+JSON.stringify(products[0][id]));
       showPage('StoreDetails');
       changeSelection();
       selectedProductId=products[0][id].id;
       selectedUnitPrice=products[0][id].clearancePrice;
       productImage=s3URL+products[0][id].images[0].thumb;
       $("#thumb1").attr("src",s3URL+products[0][id].images[0].thumb);
       $("#thumb2").attr("src",s3URL+products[0][id].images[1].thumb);
       $("#thumb3").attr("src",s3URL+products[0][id].images[2].thumb);
       $("#thumb4").attr("src",s3URL+products[0][id].images[3].thumb);
       $("#largeImg").attr("src",s3URL+products[0][id].images[0].thumb);
       $("#prodTitle1").html(products[0][id].title);
       $("#prodTitle2").html(products[0][id].title);
        $("#DprdSize").attr("selectedIndex", -1);

        $('#DprdSize').empty();

       if(products[0][id].sizes && products[0][id].sizes.length>0) {
          $.each(products[0][id].sizes, function(key, value) {  
               $('#DprdSize').append(
                                    $("<option></option>")
                                        .text(value)
                                        .val(value)
                                );
          });
       }
       else{
           $('#DprdSize').append(
                                    $("<option></option>")
                                        .text('')
                                        .val('')
                                );
       }
    

       $("#prd_colors").html('');
        $.each(products[0][id].colors, function(key, value) {  
          if(key==0) { selectedClr=value; }
        $("#prd_colors").html('<div class="prd_cr" id="actclass"><span id="0" class="act" style="background-color:'+value+';"></span></div>'); 
          });

      $("#qty1").html('1');
      $("#prodPrice").html(products[0][id].price);
      $("#productPrice").val(products[0][id].price);


}
function confirmOrder()
{
   //console.log(selectedClr);
   //console.log($("#prodPrice").html());
    if($("#confirmOrder").html()=='Continue'){
    $("#confirmOrder").html("Confirm");
    $("#chng").show();
    $("#DprdSize").hide();
    $("#qtyBlock").hide();
    $("#selectedSize").html($( "#DprdSize option:selected" ).val());
    $("#selectedQty").html($("#qty1").html());

   } else{
  
   showPage('shippingAddress');
   getAddress();

   }

}
function changeSelection()
{
   $("#confirmOrder").html("Continue");
   $("#chng").hide();
   $("#chng").hide();
   $("#DprdSize").show();
   $("#qtyBlock").show();
   $("#selectedSize").html('');
   $("#selectedQty").html('');
}
function getPhotos() {
   $('.preloader').show();

   var bgimage;
    $.ajax({
        url: serverURL + "/gets3PhotosFinal",
        type: "GET",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(data) {
              largImg = [];
              ImgTtile=[];
              imgIDs=[];
          $("#portfoliolist").html('');
            $('.preloader').hide();
            $.each(data.photos, function(key, value) {
               
      var vcount = (value.vcount ? '<li><div  id="v_'+value.id+'" ><img src="img/stats/view.png" class="statsIcon"/><span class="statsTxt">'+value.vcount+'</span></div></li>' : '<li><div  id="v_'+value.id+'" ></div></li>');
      var lcount = (value.lcount ? '<li><div id="l_'+value.id+'" ><img src="img/stats/heart.png" class="statsIcon"/><span  class="statsTxt">'+value.lcount+'</span></div></li>' : '<li><div  id="l_'+value.id+'" ></div></li>');
      var scount = (value.scount ? '<li><div id="s_'+value.id+'"  ><img src="img/stats/share.png" class="statsIcon"/><span class="statsTxt">'+value.scount+'</span></div></li>' : '<li><div  id="s_'+value.id+'" ></div></li>');

             $( "#portfoliolist" ).append('<div class="portfolio logo1 mix_all port-big-grid" data-cat="logo" style="display: inline-block; opacity: 1;"><div class="portfolio-wrapper "><a   href=javascript:showPopup(1,'+key+') class="b-link-stripe b-animate-go  thickbox swipebox"><img class="bgImg" src="img/imgBg.jpg" /><img id="PhBg'+key+'" class="p-img" src="img/imgBg.jpg" /><div class="statsWrap"><div class="IconsWrap"><ul> '+vcount+lcount+scount+'</ul></div></div></a> </div></div>');
             largImg.push(s3URL + value.mobile);
             ImgTtile.push(value.title);
             imgIDs.push(value.id);


       //$('#PhBg'+key).fadeOut(400, function() {
        bgimage = new Image();
        if (s3URL + value.thumb != null)
        bgimage.src = s3URL + value.thumb;       
        
        $(bgimage).load(function(){
         //$("#PhBg"+key).removeClass("preloader1");
            $("#PhBg"+key).fadeOut(100);
            setTimeout(function(){$("#PhBg"+key).attr("src",s3URL + value.thumb).fadeIn(400);},100);
            
         }); 

        
  
            });
        },
        error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();},
        complete: function() {
           


        }
    });
}
function gets3EventsData() {
      $('.preloader').show();
      $('#e1').html('');
      var e1="";

   var UserID = $("#user_Id").val();
              //  if(UserID) 
              //  {

                      $.ajax({
        url: serverURL + "/gets3EventsData",
        type: "GET",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(res) {

           events=[];
           events.push(res['event']);

            $.each(res['event'], function(key, value) {
            
               e1 = e1+'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 nomarg" ><div class="handsymbol et_blk" onclick="showe2('+key+')"> <div class="imgslide"> <img src="'+s3URL+value.images[0]+'" class="img-responsive" alt="'+value.title+'" /><img src="'+s3URL+value.images[1]+'" class="img-responsive" alt="'+value.title+'"/><img src="'+s3URL+value.images[2]+'" class="img-responsive" alt="'+value.title+'" /></div><div class="ev_cn"><h4>'+value.title+'</h4><h4><span>Date: </span><span>'+value.date+'</span></h4><h4><span>Price: $</span><span id="">'+value.price+'</span></h4><h5><span>Venue: </span><span>'+value.venue+'</span></h5></div> </div></div>';

               if(res['event'].length==parseInt(key+1)) {
                $('.preloader').hide();  showe1(); 

                  $('#e1').html(e1);
                                if(res['event'].length=key+1) { 
                                 showe1();
                                $('.preloader').hide(); 

                                 $('.imgslide').slick({
                            arrows: false,
                              dots: false,
                              autoplay: true,
                              autoplaySpeed: 5000,
                              fade: true,
                              speed: 1000,
                              pauseOnHover: false,
                              pauseOnDotsHover: true
                        });
               }
         }

            });      
           

        },
        error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();} 
    });


               // } else{
               //     showParingScreen();
               // }   



   /*  var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
           // var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {

              
  
   
            } else {
                
                 $('.preloader').hide();
                $( ".joinbtn" ).click(); 



             }
        }, null);
    });   */
    
 }
function gets3Files(pid) {



     var UserID = $("#user_Id").val();
              //  if(UserID) {


  $('.preloader').show();
     $.ajax({
        url: serverURL + "/gets3FilesMobile",
        type: "GET",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(res) {

                     if(pid=="GalleryPage") {
                    $("#GalleryPageContent").html('');
                    $('.preloader').hide();

 
                 $.each(res.galleryMain, function(key, value) {

                  var galIMG=s3URL+'/'+value.image;

                   $("#GalleryPageContent").append('<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 nogap"><a class="galImg" href=javascript:showPage("'+value.name.replace(/ /g, '%20')+'")><div class="overlay"></div><img  src="img/picBg.svg"  class="img-responsive gal_thumb bgImg" /><img id="GalBG'+key+'" src="img/picBg.svg" class="img-responsive gal_thumb"/><h4>'+value.name+'</h4></a></div>');
 
                   galBgimage = new Image();
                  if (galIMG != null)
                  galBgimage.src = galIMG;    

         
                 $(galBgimage).load(function(){

               $("#GalBG"+key).fadeOut(100);
                setTimeout(function(){$("#GalBG"+key).attr("src",galIMG).fadeIn(400);},100);
             
                   });   

                })  


                    } else if(pid=="CastPage") {
                    $("#castSlider").html('');
                    $('.preloader').hide();
                       $.each(res.castSlideShow, function(key, value) {

                                $("#castSlider").append('<div class="imgholder"><img src="'+cdnPath+'/'+value.image+'"><div class="grad"></div><div class="castdetails drt"><p class="titleName"><span class="casttxt">Cast &nbsp; — &nbsp; </span><span class="flm_mkr" >'+value.charactername+'</span></p><h1 class="casttitle"><span class="plus">+ </span><span>'+value.name+'</span></h1><p class="castname"><span>  </span><span> </span></p></div></div>'); 

                                if(res.castSlideShow.length==key+1) {

                                  if(castCnt==1) {$('.castSlider').slick('unslick'); castCnt=0;}


                                     $('.castSlider').slick({
                                    arrows: true,
                                      dots: false,
                                      autoplay: true,
                                      autoplaySpeed: 5000,
                                      fade: true,
                                      speed: 1000,
                                      pauseOnHover: false,
                                      pauseOnDotsHover: true
                                });
                                     castCnt++;
                                }

                             }) ; 
                    }
                else if(pid=="Event") {
                     $('.preloader').hide();
                       $("#eventPrice").html(res.partyPrice);
                       $("#ePrice").html(res.partyPrice);
                    }



        },
        error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();},
        complete: function() {


        }
    });

          //      } else{
           //         showParingScreen();
           //     }   




   /*  var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
            //var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {

              
  

            } else {
                
                 $('.preloader').hide();
                $( ".joinbtn" ).click(); 



             }
        }, null);
    });   */


    
 }
function getCarousels() {
  $('.preloader').show();
  // var UserID = $("#user_Id").val();  
   var UserID = 'guest_id';  
                                  $.ajax({
                    url: serverURL + "/getCarousels?device=IosApp",
                    type: "POST",
                    data: JSON.stringify({
                        "getCarousels": {
                            "id": UserID,
                            "userType": "device"
                        }
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function(res) {
                     $('.preloader').hide();
                     $("#GalleryPageContent").html('');
                     $("#GalleryPageContent").hide();
                    $('.preloader').hide();

 
                 $.each(res, function(key, value) {

                  // var galIMG=s3URL+'/'+value.image;
                 var galIMG='http://107.21.252.71/images/image.jpg';

                   $("#GalleryPageContent").append('<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 nogap"><a class="galImg" href=javascript:showPage("'+value.carousel_actualname.replace(/ /g, '%20')+'")><div class="overlay"></div><img  src="img/picBg.svg"  class="img-responsive gal_thumb bgImg" /><img id="GalBG'+key+'" src="img/picBg.svg" class="img-responsive gal_thumb"/><h4>'+value.carousel_actualname+'</h4></a></div>');
 
                   galBgimage = new Image();
                  if (galIMG != null)
                  galBgimage.src = galIMG;    

         
                 $(galBgimage).load(function(){

               $("#GalBG"+key).fadeOut(100);
                setTimeout(function(){$("#GalBG"+key).attr("src",galIMG).fadeIn(400);},100);
             
                   });   

                })  

             
                 $("#GalleryPageContent").show();
                 
                 //showPage('GalleryPage');


                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    $('.preloader').hide();
                    } 
                });
               

                  
  

}

function prevImage() {
    $("#galleryBlock").carousel('prev');
}


function nextImage() {
    $("#galleryBlock").carousel('next');
}
 

$( document ).ready(function() {
   

 

$( "#myBtn" ).click(function() { 
 if($('.toggleHide').css('display')=='none')
 {
  $('.toggleHide').show();
  $("#myBtn").html('CLOSE');
 }
 else{
  $('.toggleHide').hide();
  $("#myBtn").html('PAIR DEVICES');
 }

 });

$( "#Addr" ).click(function() { 
  $("#addrBlock").show();

 });


$( ".logout" ).click(function() {

    var myDB = window.sqlitePlugin.openDatabase({ name: 'AeomDB.db', location: 'default' }, null, null);
      myDB.transaction(function(tx) {
          var qry = "DELETE FROM aeomSettings";
          tx.executeSql(qry, function() {
              // console.log("Delete success");

          });
      }, function errorCB(err) {
          alert("Error processing SQL: " + err.code);
      }, function() {
         
                     $(".joinbtn").show();
                     $('#profile_pic').attr('src', 'img/user.jpg');
                     $('.uAvatar').hide();
                     showPage('HomePage');
 
      });


});


 

/// Video Upload 
 







});

function getuserdetails() {
      $("#loading").show();

   var UserID = $("#user_Id").val();
                if(UserID) {

                     $.ajax({
                    type: "POST",
                    url: serverURL + "/getUser",
                    data: JSON.stringify({
                        "getUser": {
                            "userId": UserID,
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                            $("#useremail").html(data.User.email);
                           $("#email_shipping").html(data.User.email);
                           $("#email").val(data.User.email);
                        $("#plantype").html(data.User.subscription_type);
                        $("#planperiod").html(data.User.subtext);
                        $("#userName").val(data.User.full_name);
                        $("#customer_id").val(data.User.customer_id);
                        $("#user_Id").val(data.User._id);
                        //$("#cancel_membership").html(data.User.canceltext);
                        //$("#customeID").val(data.User.customer_id);
                        //$("#UID").val(data.User._id);
                        //$("#subscription_end").val(data.User.subscription_end);
                        //if (data.User.cancel_subscription == true || data.User.subscription_type == 'free') {
                        //    $("#cancelMember").hide();

                       // } else {
                        //    $("#cancelMember").show();
                       // }

                        $("#loading").hide();

                      

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
 
                    },
                    complete: function() {}
                });

                } else{
                    showParingScreen();
                }   



   /* var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
            //var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
              //  var UserID = results.rows.item(i).key3;
                var UserID = $("#user_Id").val();

             

            } else {
              }
        }, null);
    });  */

}


function addbuttonclick() {
    $("#loading").show();
     $("#statusupdate").html("");

         var UserID = $("#user_Id").val();
                if(UserID) {

                 var devicecode = $("#devicecode").val();
 
                $.ajax({
                    type: "POST",
                    url: serverURL + "/addDevice",
                    data: JSON.stringify({
                        "addDevice": {
                            "code": devicecode,
                            "account": {
                                "_id": UserID
                            }
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                       $("#statusupdate").html(data.msg);
                        $("#statusupdate").show();
                        setTimeout(function(){$("#statusupdate").hide(); },4000);
                        $("#devicecode").val("");
                        $("#loading").hide();
                        getdevicepaired();
                        $( "#myBtn" ).click();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();

                    },
                    complete: function() {


                    }
                });

                } else{
                   showParingScreen();
                }   


   /* var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
            //var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
                //var UserID = results.rows.item(i).key3;
                var UserID = $("#user_Id").val();


            } else {
                 
                Logout();
            }
        }, null);
    });  */
}

function getdevicepaired() {
   
   var UserID = $("#user_Id").val();
                if(UserID) {
  $.ajax({
                    type: "POST",
                    url: serverURL + "/fetchPairedDevice",
                    data: JSON.stringify({
                        "fetchPairedDevice": {
                            "user_id": UserID,
                            "id": UserID,
                            "userType": 'web'
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {

                        if (data.pairedDevice.length > 0) {

                            $("#devicedetails").empty();
                            var datahtml = '<table width="100%" border="0" cellspacing="4" cellpadding="4"><tr><th width="90%" class="textredcolor">Device Id </th><th width="10%"></th></tr>';

                            for (i = 0; i < data.pairedDevice.length; i++) {
                                var iddevice = data.pairedDevice[i].device_id;
                                var _iddev = data.pairedDevice[i]._id;
                                var start_timedev = data.pairedDevice[i].start_time;

                                datahtml += '<tr><td>' + iddevice + '<br/><span class="colordarkgrey">' + start_timedev + '</span></td><td><i class="fa fa-times buttoncancelround" onclick=removepairedevice("' + _iddev + '")></i></td></tr>';
                                if (data.pairedDevice.length == i + 1) {
                                    datahtml += '</table>';

                                    $("#devicedetails").html(datahtml);
                                }
                            }
                        } else {
                            $("#devicedetails").html('No Paired Devices');

                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#devicedetails").empty();
                     },
                    complete: function() {


                    }
                });

                } else{
                    showParingScreen();
                }   


  /*  var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
           // var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
               //var UserID = results.rows.item(i).key3;
 				var UserID = $("#user_Id").val();
               

            } else {
                 Logout();
            }
        }, null);
    });  */
}

function removepairedevice(deviceid) {
    $("#loading").show();
    $("#statusremoveupdate").html("");


       var UserID = $("#user_Id").val();
                if(UserID) {
    $.ajax({
                    type: "POST",
                    url: serverURL + "/removePairedDevice",
                    data: JSON.stringify({
                        "removePairedDevice": {
                            "_id": deviceid,
                        }
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                        $("#statusremoveupdate").html("Device Removed Successfully");
                         setTimeout(function(){$("#statusremoveupdate").hide(); },4000);
                        $("#loading").hide();
                        getdevicepaired();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#loading").hide();
                     },
                    complete: function() {


                    }
                });

                } else{
                    showParingScreen();
                }   



   /* var myDB = window.sqlitePlugin.openDatabase({name: 'AeomDB.db', location: 'default'}, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
            //var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
               // var UserID = results.rows.item(i).key3;
				var UserID = $("#user_Id").val();

            

            } else {
                 Logout();
            }
        }, null);
    });  */
}

  
function stopAudio(){

    if($("#playIcon").attr("class")=='play ui-link playAudio') {  $( ".play" ).click(); }  

    
     $('#playerContainer').hide();

}

function getCarouselsList(id) { 

 
      $('.preloader').show();
      $("#vList").html('');
      $("#vTitle").html('');

    var CourosalID = id.replace(/ /g, '');

       var UID = $("#user_Id").val();
       if(UID) {  var UserID = UID;} else { var UserID = 'guest_id';} 

                 // if(UserID) 
               // {


                var NavItems = [];
               // var UserID = results.rows.item(i).key3;
                 $.ajax({
                    url: serverURL + "/getCarouselData",
                    type: "POST",
                    data: JSON.stringify({
                        "getCarouselData": {
                            "name": id,
                            "id": UserID,
                            "userType": "web"
                        }
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function(imgData) {
                            $('.preloader').hide();
                            

                              if(id=='Music'){ 
                                 if (imgData[id] && imgData[id].length > 0) { 
                                    $("#playListContainer").html("");
                                    $.each(imgData[id], function(key, value) {

                                        $("#playListContainer").append('<li class="atl" data-src="'+value.url+'"><div class="qw-track-play"> <a class="playable-mp3-link"  href="javascript:void(0)"><span class="qticon-caret-right"></span></a></div> <div class="qw-track-text"> <p class="qw-track-name"> '+value.name+'</p><p class="qw-artists-names qw-small qw-caps"><a href="javascript:void(0)" class="qw-artistname-title art_title">Tupac</a></p> </div></li>');

                                       if(imgData[id].length==key+1) { 

                                         $("#playListContainer").audioControls({
                                                  autoPlay : false,
                                                  timer: 'increment',
                                                  onAudioChange : function(response){
                                                    $('.songPlay').text(response.title + ' ...');
                                                  },
                                                  onVolumeChange : function(vol){
                                                    var obj = $('.volume');
                                                    if(vol <= 0){
                                                      obj.attr('class','volume mute');
                                                    } else if(vol <= 33){
                                                      obj.attr('class','volume volume1');
                                                    } else if(vol > 33 && vol <= 66){
                                                      obj.attr('class','volume volume2');
                                                    } else if(vol > 66){
                                                      obj.attr('class','volume volume3');
                                                    } else {
                                                      obj.attr('class','volume volume1');
                                                    }
                                                  }
                                                });

                                        $( ".atl" ).click(function() {
                                          $('#playerContainer').show();

                                      });

                                       }; 
                                    });

                                }
                              } else {
 
                                   $("#vTitle").html(id);
                             if (imgData[id] && imgData[id].length > 0) {
                              //  for (i = 0; i < imgData[id].length; i++) {
                                 $.each(imgData[id], function(key, value) {
                                  if(value.url_m3u8)
                              {
                                var videoLink=value.url_m3u8;
                              }
                               else if(value.url)
                              {
                                var videoLink=value.url;
                              }
                              else
                              {
                                var videoLink="";
                              }

                               var vImg=value.metadata.movie_art; 
                                // $("#vList").append('<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><a href=javascript:playVideo("'+videoLink+'")><img src="img/playicon.png" class="img-responsive playIcn" /></a><a href="#" class="btsthumb relpos"><img src="img/blackBg.png" class="img-responsive bgImg"/><img id="VBG'+key+'" src="img/blackBg.png" class="img-responsive"/></a><h6 class="btsTitle">'+value.name+'</h6></div>');
                                 $("#vList").append('<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><a href=javascript:showPage1("'+value.id+'") class="btsthumb relpos"><img src="img/blackBg.png" class="img-responsive bgImg"/><img id="VBG'+key+'" src="img/blackBg.png" class="img-responsive"/></a><h6 class="btsTitle">'+value.name+'</h6></div>');

                             
                   VideoThumb = new Image();
                  if (vImg != null)
                  VideoThumb.src = vImg;    
                 $(VideoThumb).load(function(){
                 $("#VBG"+key).fadeOut(100);
                 setTimeout(function(){$("#VBG"+key).attr("src",vImg).fadeIn(400);},100);  

                               });


                              })  // for end
                             } }
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();},
                     complete: function() {


                             $.ajax({
                    url: serverURL + "/gets3filesmobile",
                    type: "GET",
                    data: "",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function(data) {
                      
                      //console.log(data.galleryMain[1].name);
                    //  console.log(serverURL+'/'+data.galleryMain[1].image)
                                 $("#vList").append('<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><a href=javascript:showPage("Photos") class="btsthumb relpos"><img src="img/blackBg.png" class="img-responsive bgImg"/><img id="photoBG" src="img/blackBg.png" class="img-responsive"/></a><h6 class="btsTitle">'+data.galleryMain[1].name+'</h6></div>');  
               var  pImg=s3URL+'/'+data.galleryMain[1].image;
                     VideoThumb = new Image();
                  if (pImg != null)
                  VideoThumb.src = pImg;    
                 $(VideoThumb).load(function(){
                 $("#photoBG").fadeOut(100);
                 setTimeout(function(){$("#photoBG").attr("src",pImg).fadeIn(400);},100);  

                               });

                    },
                    error: function(jqXHR, textStatus, errorThrown) {  
                    }
                });

                     }

                    

                });


                //} else{
                    // show pairing screen
               // }   

 

}

function getAssetData(id) {
  $('.preloader').show();
   $("#vsTitle").html('');
   $("#vsList").html('');
   var UserID = $("#user_Id").val();  
                //if(UserID)
               //  {

                $.ajax({
                    url: serverURL + "/getAssestData",
                    type: "POST",
                    data: JSON.stringify({
                        "getAssestData": {
                            "videoId": id,
                             "userId": UserID
                          }
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function(imgData) {
                       $('.preloader').hide();
                       $("#vsTitle").html(imgData.name);  
                     // console.log(imgData.contains);
                           if(imgData.status==false || imgData.status=='false'){
                            showParingScreen();



                          } else {
                               if (imgData && imgData.contains.length > 0) {
 


                                 $.each(imgData.contains, function(key, value) {

                                  if(value.url_m3u8)
                              {
                                var videoLink=value.url_m3u8;
                              }
                               else if(value.url)
                              {
                                var videoLink=value.url;
                              }
                              else
                              {
                                var videoLink="";
                              }

                               var vImg=value.metadata.movie_art; 

                                 var vImg=value.metadata.movie_art; 
                               $("#vsList").append('<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><a href=javascript:playVideo("'+videoLink+'")><img src="img/playicon.png" class="img-responsive playIcn" /></a><a href="#" class="btsthumb relpos"><img src="img/blackBg.png" class="img-responsive bgImg"/><img id="VSBG'+key+'" src="img/blackBg.png" class="img-responsive"/></a><h6 class="btsTitle">'+value.name+'</h6></div>');


                                VideoThumb = new Image();
                                if (vImg != null)
                                VideoThumb.src = vImg;    
                               $(VideoThumb).load(function(){
                               $("#VSBG"+key).fadeOut(100);
                               setTimeout(function(){$("#VSBG"+key).attr("src",vImg).fadeIn(400);},100);  

                               });



                                 })  // for end


                         }
                          }

                       

                    },
                    error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();}
                });


                //} else{
                    // show pairing screen
               // }   



 

}

function showParingScreen()
{
                              showPage('PairingScreen');

                              $.ajax({
                                           url: serverURL+"/getActivationCode",
                                          type: "POST",
                                          data: JSON.stringify({
                                              "getActivationCode": {
                                                  "model": device.model,
                                                  "manufacturer": device.manufacturer,
                                                  "device_name": device.platform,
                                                  "device_id": device.uuid,
                                                  "device_mac_address": '',
                                                  "brand_name": device.platform,
                                                  "host_name": '',
                                                  "display_name": device.platform,
                                                  "serial_number": device.serial 
                                               }
                                          }),
                                          dataType: "json",
                                          contentType: "application/json; charset=utf-8",
                                          traditional: true,
                                          success: function(data) {
                                            $("#loading").hide();
                                                var Pairingcode=data.code;
                                                $("#pairCode").html("");
                                               if(data.pair_devce=='pending'){
                                                    showPage('PairingScreen');
                                                    console.log(Pairingcode);
                                                    $("#user_Id").val('');   
                                                      for(i = 0; i < Pairingcode.length; i++ )
                                                    {
                                                       $("#pairCode").append('<li>'+Pairingcode.charAt(i)+'</li>'); 
                                                    }
                                                      setTimeout( function(){ getParingStatus()},5000);

                                                }
                                               else
                                               {
                                                       $("#user_Id").val(data.user_id); 
                                                        showPage('HomePage');
                                                        $('.navbar').show();
                                               }
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                             // alert(errorThrown);
                                             $("#loading").hide();
                                          }

                                      });
}
                    function getParingStatus(){
                           
                              $.ajax({
                                           url: serverURL+"/getActivationCode",
                                          type: "POST",
                                          data: JSON.stringify({
                                              "getActivationCode": {
                                                  "model": device.model,
                                                  "manufacturer": device.manufacturer,
                                                  "device_name": device.platform,
                                                  "device_id": device.uuid,
                                                  "device_mac_address": '',
                                                  "brand_name": device.platform,
                                                  "host_name": '',
                                                  "display_name": device.platform,
                                                  "serial_number": device.serial 
                                               }
                                          }),
                                          dataType: "json",
                                          contentType: "application/json; charset=utf-8",
                                          traditional: true,
                                          success: function(data) {
                                                    if(data.pair_devce=='active'){
                                                        $("#user_Id").val(data.user_id);   
                                                        showPage('HomePage');
                                                        $('.navbar').show();
                                                }
                                                else
                                                {
                                                  setTimeout( function(){ getParingStatus()},5000);
                                                }
                                          },
                                          error: function(jqXHR, textStatus, errorThrown) {
                                          // alert(errorThrown);

                                          } 
                                      });   
}
function getTrailer(id) {
  $('.preloader').show();

   var UserID = $("#user_Id").val();
                if(UserID) {

                $.ajax({
                    url: serverURL + "/getCarouselData",
                    type: "POST",
                    data: JSON.stringify({
                        "getCarouselData": {
                            "name": id,
                            "id": UserID,
                            "userType": "web"
                        }
                    }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    success: function(imgData) {
                      $('.preloader').hide();
                          if (imgData[id] && imgData[id].length > 0) {
 

                           // for (i = 0; i < imgData[id].length; i++) {
 
                              if(imgData[id][0].url_m3u8)
                              {
                                var videoLink=imgData[id][0].url_m3u8;
                              }
                               else if(imgData[id][0].url)
                              {
                                var videoLink=imgData[id][0].url;
                              }
                              else
                              {
                                var videoLink="";
                              }

                                playVideo(videoLink)

                          //  }
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) { $('.preloader').hide();}
                });


                } else{
                   showParingScreen();
                }   




   /* var myDB = window.sqlitePlugin.openDatabase({ name: 'AeomDB.db', location: 'default' }, null, null);
    myDB.transaction(function(transaction) {
        transaction.executeSql('SELECT * FROM aeomSettings', [], function(tx, results) {
           // var len = results.rows.length,
            var len = 1,
                i = 0;
            if (len > 0) {
                var NavItems = [];
                //var UserID = results.rows.item(i).key3;
                var UserID = $("#user_Id").val();



            } else {
                console.log('No user found.');
                $('.preloader').hide();
                $( ".joinbtn" ).click(); 
            }
        }, null);
    }); */

}
  function getLatLan()
  {

 var onSuccess = function(position) {
    /*    alert('Latitude: '          + position.coords.latitude          + '\n' +
              'Longitude: '         + position.coords.longitude         + '\n' +
              'Altitude: '          + position.coords.altitude          + '\n' +
              'Accuracy: '          + position.coords.accuracy          + '\n' +
              'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
              'Heading: '           + position.coords.heading           + '\n' +
              'Speed: '             + position.coords.speed             + '\n' +
              'Timestamp: '         + position.timestamp                + '\n');*/
              alert(position.coords.latitude);
    $("#lat").val(position.coords.latitude);
    $("#lan").val(position.coords.longitude);
     };


    // onError Callback receives a PositionError object
    //
    function onError(error) {  alert(error);
        /*alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');*/
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);

  }

  function closeModal() {  
      //$("#modal").hide();
    //  $("#hoverTxt").css("background-color", "#D3D3D3");
      $("#cropLoader").hide();
    //  $("#hoverTxt").val('');
      $('#modal').modal('hide');
      $('#uimagediv').html('<div class="imgholderu" align="center"><input type="file" class="upload" onChange="readURL(this);"/></div>');

      $('[type="file"]').ezdz({
          text: 'Upload or Drag your Photo Inside the box',

          reject: function(file, errors) {
              if (errors.mimeType) {
                  alert(file.name + ' must be an image.');
              }
          }
      });
      $(".divcentersave").hide();
      $("#widget").hide();
      $('#uimagediv').show();
      $('#memesavediv').html('<div id="img-out"></div>');
      $('#memesavediv').hide();
      $('#shareIcons').hide();
  }


function playVideo(videoUrl) {

     var videoUrl = videoUrl.replace("https:", "http:");
      var options = {
                    seekTime: 0,
                    successCallback: function(obj) {
                      //  console.log(obj.pos);
                         
                    },
                    progressCallback: function(obj) {

                    },
                    errorCallback: function(errMsg) {
                        console.log("Error! " + errMsg);
                    },
                    orientation: 'landscape'
                };
                window.plugins.streamingMedia.playVideo(videoUrl, options);
}
 
 function fbShare(){
  


      if($('.slick-active img:eq(1)').attr('data-lazy'))
                         {
                              ci=largImg.indexOf( $('.slick-active img:eq(1)').attr('data-lazy') ) ;
                         }
                         else
                         {
                            ci=largImg.indexOf( $('.slick-active img:eq(1)').attr('src') ) ;
                         }

            ShareCount(ci,'facebook');
                           
    galCurrentImage=$('.slick-active img:eq(1)').attr('src');
     cordova.InAppBrowser.open('https://www.facebook.com/sharer/sharer.php?caption=AllEyezOnMe&title=AllEyezOnMe&description=AllEyezOnMe&u='+galCurrentImage, 'Share Facebook', 'height=800, width=800');
};

 function plusShare(){
   // cordova.InAppBrowser.open('https://plus.google.com/share?title=AllEyezOnMe&summary=AllEyezOnMe&description=AllEyezOnMe&summary=AllEyezOnMe&url='+url, 'Share Google +', 'height=800, width=800');
         if($('.slick-active img:eq(1)').attr('data-lazy'))
                         {
                              ci=largImg.indexOf( $('.slick-active img:eq(1)').attr('data-lazy') ) ;
                         }
                         else
                         {
                            ci=largImg.indexOf( $('.slick-active img:eq(1)').attr('src') ) ;
                         }

            ShareCount(ci,'GooglePlus');
    galCurrentImage=$('.slick-active img:eq(1)').attr('src');
    cordova.InAppBrowser.open('https://plus.google.com/share?url='+galCurrentImage, 'Share Google +', 'height=800, width=800');
  };


function ShareCount(id,type)
{
   // console.log(imgIDs[id]);
    console.log(type);
    $.ajax({
                            url: serverURL + "/Shares",
                            type: "POST",
                            data: JSON.stringify({
                                  "Shares": {
                                      "id": imgIDs[id],
                                      "type": "photo",
                                      "share": type,
                                      "count": "1"
                                  }
                              }),
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            traditional: true,
                            success: function(data) {
                                         $("#s_"+data.shares.sourceId).html('<img src="img/stats/share.png" class="statsIcon"/><span class="statsTxt">'+parseInt(data.shares.count)+'</span>');
                                        
                                
                            },
                            error: function(jqXHR, textStatus, errorThrown) {  },
                            
                        });  

}




